import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Vue2TouchEvents from 'vue2-touch-events'




import Game from './assets/pages/game.vue'
import Final from './assets/pages/final.vue'

Vue.use(VueRouter)
Vue.use(Vue2TouchEvents)



const routes = [
  { path: '/game', component: Game },
  { path: '/', component: App },
  { path: '/final', name:'final',component: Final , props: true},
]
const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes
})
Vue.config.productionTip = false

new Vue({
  router,
}).$mount('#app')
